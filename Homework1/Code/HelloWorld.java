public class HelloWorld {
    public static void main(String[] args) {

    // Simple java program printing "Hello World" in ACII art
            System.out.println(""
                    + " _   _        _  _           _      _          "
                    + "     "
                    + "_       _  _ ");
            System.out.println("| | | |      | || |         | |    | |             | |     | || |");
            System.out.println("| |_| |  ___ | || |  ____   | |    | |  ____  _ __ | |  ___| || |");
            System.out.println("|  _  | / _ \\| || | / _  \\  | | _  | | / _  \\| '__/| | /" 
                    +"  _  ||_|");
            System.out.println("| | | ||  __/| || || |_| |  | \\/ \\/  || |_| || |   | ||"
                    + "  (_| | _");
            System.out.println("|_| |_| \\___||_||_| \\___/    \\__/\\__/  \\___/ |_|   |_|"
                    + " \\_____||_|");
    }
}

+++++++++++++++++++++++++++++++++++++++++
README file for Arely Miramontes Math 471
+++++++++++++++++++++++++++++++++++++++++

1. Homework 1:

   - The report is in Homework1 and is called report.txt

   - The code for this homework is located in Homework1/Code and is
     documented in the appendix of the report.

2. Homework 2:

   - The report and readme are in codes/fortran/newton

   - The code for this homework is located also in codes/fortran/newton
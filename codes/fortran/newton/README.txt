++++++++++++++++++++++++++++++++++++++++++++++++++++
		      HOMEWORK 2
		        README
	     Arely and Berenise Miramontes
++++++++++++++++++++++++++++++++++++++++++++++++++++

1. The report and code files can be found under
   codes/fortran/newton

2. Run newtonS.p by entering "perl newtonS.p" in the
   command line

3. Open the Report by entering "open REPORT.txt" in
   the command line to view our summary and findings
   in this homework assignment
